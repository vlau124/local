-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2015 at 05:57 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vluxtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `ID` int(11) NOT NULL,
  `Question` text NOT NULL,
  `Income_Tax` int(11) NOT NULL,
  `Education` int(11) NOT NULL,
  `Pubic_Health` int(11) NOT NULL,
  `Entrepreneurship` int(11) NOT NULL,
  `Community_Art` int(11) NOT NULL,
  `Immigration` int(11) NOT NULL,
  `Income_Tax2` int(11) NOT NULL,
  `Education2` int(11) NOT NULL,
  `Pubic_Health2` int(11) NOT NULL,
  `Entrepreneurship2` int(11) NOT NULL,
  `Community_Art2` int(11) NOT NULL,
  `Immigration2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`ID`, `Question`, `Income_Tax`, `Education`, `Pubic_Health`, `Entrepreneurship`, `Community_Art`, `Immigration`, `Income_Tax2`, `Education2`, `Pubic_Health2`, `Entrepreneurship2`, `Community_Art2`, `Immigration2`) VALUES
(1, 'Should we build more schools?', 5, 3, 2, 3, 3, 0, 3, 4, 5, 1, 4, 0),
(2, 'Muslims need to be banned', 5, 1, 0, 0, 0, 0, 0, 3, 0, 0, 0, 5),
(3, 'Should America go to war?', 5, 2, 0, 5, 0, 0, 0, 2, 0, 0, 0, 3),
(4, 'We need to invest in renewable energy', 3, 3, 3, 3, 0, 0, 2, 0, 0, 0, 0, 0),
(5, 'We need Bill Gates to shut down the Internet!', 5, 0, 0, 0, 0, 0, 0, 5, 2, 5, 3, 0),
(6, 'Guns should be banned!', 3, 1, 2, 0, 0, 0, 2, 1, 2, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
