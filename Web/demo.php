<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Political Debate</title>

    <!-- css -->
    <!-- 3.3.6 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />

    <!-- scripts -->
    <!-- 2.1.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- 3.3.6 -->
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="index.html">Home</a></li>
                  <li><a href="Quiz.html">Political Debate</a></li>
                  <li><a href="edit.php">Edit</a></li>
                  <li><a href="del.php">Delete</a></li>
              </ul>
          </div>
      </div>
  </nav>
  
  <br>
  <br>
  <br>
  <br>

<?php
//dumping everything into variables to be easily changed
define('DB_NAME', 'vluxtest');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');

//connecting to the database
$link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);

//if there are issues connecting display error message
//checks to see if we are connected
if (!$link) {
	die('Could not connect: ' . mysql_error());
}

$db_selected = mysql_select_db(DB_NAME, $link);

if (!$db_selected) {
	die('Can\'t use ' . DB_NAME . ': ' . mysql_error());
}

//putting everything into variables again of 
//what i want to get and store
// post grabs the form data
$value = $_POST['Question'];

$value2 = $_POST['Income_Tax'];
$value3 = $_POST['Education'];
$value4 = $_POST['Pubic_Health'];
$value5 = $_POST['Entrepreneurship'];
$value6 = $_POST['Community_Art'];
$value7 = $_POST['Immigration'];

$value8 = $_POST['Income_Tax2'];
$value9 = $_POST['Education2'];
$value10 = $_POST['Pubic_Health2'];
$value11 = $_POST['Entrepreneurship2'];
$value12 = $_POST['Community_Art2'];
$value13 = $_POST['Immigration2'];

//inserting the data into the proper mysql data base columns
$sql = "INSERT INTO questions (Question, Income_Tax, Education, Pubic_Health, Entrepreneurship, Community_Art, Immigration, Income_Tax2, Education2, Pubic_Health2, Entrepreneurship2, Community_Art2, Immigration2)
 VALUES ('$value', '$value2', '$value3', '$value4', '$value5', '$value6', '$value7', '$value8', '$value9', '$value10', '$value11', '$value12', '$value13')";

//error message if something goes wrong
if (!mysql_query($sql)) {
	die('Error: ' . mysql_error());
}

mysql_close();
?>


<!-- displays what was posted into the mysql database -->
Success! The Following has been saved<br><br>

Question: <?php echo $_POST["Question"]; ?><br><br>

Yes Section<br>
Income Tax: <?php echo $_POST["Income_Tax"]; ?><br>
Education: <?php echo $_POST["Education"]; ?><br>
Pubic Health: <?php echo $_POST["Pubic_Health"]; ?><br>
Entrepreneurship: <?php echo $_POST["Entrepreneurship"]; ?><br>
Community Art: <?php echo $_POST["Community_Art"]; ?><br>
Immigration: <?php echo $_POST["Immigration"]; ?><br><br>

No Section:<br>
Income Tax: <?php echo $_POST["Income_Tax2"]; ?><br>
Education: <?php echo $_POST["Education2"]; ?><br>
Pubic Health: <?php echo $_POST["Pubic_Health2"]; ?><br>
Entrepreneurship: <?php echo $_POST["Entrepreneurship2"]; ?><br>
Community Art: <?php echo $_POST["Community_Art2"]; ?><br>
Immigration: <?php echo $_POST["Immigration2"]; ?><br>
<br>
<br>
  <a href="edit.php">
    <button>Click here to go back</button>
  </a> 


<!-- 1. converts the mysql database array into a readable format JSON -->
<!-- 2. writes the JSON string into a file to be used locally -->
<!-- this was orginally done in another file if you are wondering why i opened the connections twice -->

<?php
    //open connection to mysql db
    $connection = mysqli_connect("localhost","root","","vluxtest") or die("Error " . mysqli_error($connection));

    //fetch table rows from mysql db
    $sql = "select * from questions";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    
    //prints out the json encoding
    //echo json_encode($emparray);
    
    //write to json file
    $fp = fopen('data.json', 'w');
    fwrite($fp, json_encode($emparray));
    fclose($fp);
    
    echo "JSON Update Successful";
    
    //close the db connection
    mysqli_close($connection);
?>

</body>
</html>

