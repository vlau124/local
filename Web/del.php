<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Political Debate</title>

    <!-- css -->
    <!-- 3.3.6 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />

    <!-- scripts -->
    <!-- 2.1.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- 3.3.6 -->
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="index.html">Home</a></li>
                  <li><a href="Quiz.html">Political Debate</a></li>
                  <li><a href="edit.php">Edit</a></li>
                  <li><a href="del.php">Delete</a></li>
              </ul>
          </div>
      </div>
  </nav>
  <br>
  <br>
  <!-- forms stores everything into the proper name or fields-->
  <form action="delete.php" method="post">
    <h1>Which Question would you like to delete?</h1>
    <br>
    <p>Please Input ID number here </p>
    <br><br>
    <p>Delete Question ID: <input type="text" name="QuestionID"></p>

    <input type="submit" value="submit">
  </form>
  <br>
  <br>
  <br>


<script type="text/javascript">
$(document).ready(function (){
    $("#btn382").click(function(){        
        /* set no cache */
        $.ajaxSetup({ cache: false });
 
        $.getJSON("data.json", function(data){ 
            var html = [];
 
            /* loop through array */
            $.each(data, function(index, d){            
                html.push("ID ", d.ID, " ", "Question", d.Question, "<br>");
            });
 

            $("#div381").html(html.join(''));
        }).error(function(jqXHR, textStatus, errorThrown){ /* assign handler */
            /* alert(jqXHR.responseText) */
            alert("error occurred!");
        });
    });
});
</script>
 
<!-- HTML -->
<a name="#ajax-getjson-example"></a>
<div id="example-section38">    
    <h1>Database</h1>
    <br>
    <div id="div381"></div>
    <button id="btn382" type="button">Click to load data base (json type)</button>    
</div>




<br>
<br>
<br>
</body>
</html>