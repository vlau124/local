<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Political Debate</title>

    <!-- css -->
    <!-- 3.3.6 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />

    <!-- scripts -->
    <!-- 2.1.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- 3.3.6 -->
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="index.html">Home</a></li>
                  <li><a href="Quiz.html">Political Debate</a></li>
                  <li><a href="edit.php">Edit</a></li>
                  <li><a href="del.php">Delete</a></li>
              </ul>
          </div>
      </div>
  </nav>
  <br>
  <br>
  <!-- forms stores everything into the proper name or fields-->
  <form action="demo.php" method="post">
    <h1>Add your questions and stats information here:</h1>
    <p>If there is no value place 0 instead</p>
    <br><br>
    <p>Question1: <input type="text" name="Question"></p>
    <br><br>
    <br>
    <p>Yes Section:</p>
    <br><br>
    <p>Income Tax: <input type="text" name="Income_Tax"></p>
    <br><br>
    <p>Education: <input type="text" name="Education"></p>
    <br><br>
    <p>Pubic Health: <input type="text" name="Pubic_Health"></p>
    <br><br>
    <p>Entrepreneurship: <input type="text" name="Entrepreneurship"></p>
    <br><br>
    <p>Community Art: <input type="text" name="Community_Art"></p>
    <br><br>
    <p>Immigration: <input type="text" name="Immigration"></p>
    <br><br>
    <br><br>
    <p>No Section:</p>
    <br><br>
    <p>Income Tax: <input type="text" name="Income_Tax2"></p>
    <br><br>
    <p>Education: <input type="text" name="Education2"></p>
    <br><br>
    <p>Pubic Health: <input type="text" name="Pubic_Health2"></p>
    <br><br>
    <p>Entrepreneurship: <input type="text" name="Entrepreneurship2"></p>
    <br><br>
    <p>Community Art: <input type="text" name="Community_Art2"></p>
    <br><br>
    <p>Immigration: <input type="text" name="Immigration2"></p>
    <br><br>
    

    <br>
    
    <input type="submit" value="submit">
  </form>
<br>
<br>
<br>
</body>
</html>