<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Political Debate</title>

    <!-- css -->
    <!-- 3.3.6 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />

    <!-- scripts -->
    <!-- 2.1.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- 3.3.6 -->
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav">
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="index.html">Home</a></li>
                  <li><a href="Quiz.html">Political Debate</a></li>
                  <li><a href="edit.php">Edit</a></li>
                  <li><a href="del.php">Delete</a></li>
              </ul>
          </div>
      </div>
  </nav>
  
  <br>
  <br>
  <br>
  <br>
  
<?php
$db_user = 'root';
$db_pass = '';
$db_name = 'vluxtest';
$db_host = 'localhost';

//questions name of the database
$value = $_POST['QuestionID'];
$query = ("DELETE FROM questions WHERE ID=". $value);

//creating a connection
$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_name);
//incase of errors
if ($mysqli->connect_error) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$result = $mysqli->query($query);

print_r($result);

echo "Delete has been successful";

?>

<!-- 1. converts the mysql database array into a readable format JSON -->
<!-- 2. writes the JSON string into a file to be used locally -->
<!-- this was orginally done in another file if you are wondering why i opened the connections twice -->

<?php
    //open connection to mysql db
    $connection = mysqli_connect("localhost","root","","vluxtest") or die("Error " . mysqli_error($connection));

    //fetch table rows from mysql db
    $sql = "select * from questions";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    
    //prints out the json encoding
    //echo json_encode($emparray);
    
    //write to json file
    $fp = fopen('data.json', 'w');
    fwrite($fp, json_encode($emparray));
    fclose($fp);
    
    echo "JSON Update Successful";
    
    //close the db connection
    mysqli_close($connection);
?>

<br>
<br>
  <a href="del.php">
    <button>Click here to go back</button>
  </a> 

</body>
</html>